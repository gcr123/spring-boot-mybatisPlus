package com.mp.mp.entity;

import lombok.Data;

@Data
public class User {

    private int id;
    private String name;
    private int age;
    private String email;
}
