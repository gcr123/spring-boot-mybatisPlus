package com.mp.mp.controller;

import com.mp.mp.dao.UserMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class IndexController {


    @Resource
    UserMapper userMapper;

    @RequestMapping("/")
    public String index() {
        return "基础sb配mybatisPlus";
    }

    @RequestMapping("/index")
    public ModelAndView index2(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");

        return modelAndView;
    }

    @RequestMapping("/find")
    @ResponseBody
    public Object find() {
        List ls = userMapper.selectList(null);
        return ls;
    }
}
