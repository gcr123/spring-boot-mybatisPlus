package com.mp.mp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mp.mp.dao.UserMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import java.util.List;
@Controller
public class DateController {

    @Resource
    UserMapper userMapper;


    @RequestMapping("/stu")
    @ResponseBody
    public Object findDate(){
        System.out.println(("----- selectAll method test ------"));
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.select("name,age");//查询特定的列
        List userList = userMapper.selectList(queryWrapper);
        return userList;


    }
}
